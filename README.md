Matching Stars
Copyright (c) Team Rogues 2016 

Matching Stars is a puzzle game combining match-three gameplay with a combat
meta game, built for the Android platform. It is written in C/C++.

License

Matching Stars is free software licensed under the GNU General Public License
version 3.

The original non-source code parts (images, sound) of the program are
licensed under CC BY 4.0, which can be found at
https://creativecommons.org/licenses/by/4.0/. This includes all of the files
in the 'assets' subdirectory, except for the file electrolize.ttf, 
which is licensed under the Open Font License.

Compilation and building

To compile the program, you will need to correctly install the Android Software
Development Kit (SDK), Android Native Development Kit (NDK) and the Apache Ant
build system. Android API version 19 is used, so that is what needs to be
installed through the SDK's package manager.

Compilation itself is accomplished by running the 'ndk-build' tool of the
native development hit in the root directory. To build a debug version, modify
the Android.mk makefile in the jni/src directory so that the variable
"LOCAL_CFLAGS" points to "DEBUG_C_FLAGS" - for a release version, set the
variable to point to "OPTIMIZED_C_FLAGS" instead.

To install the compiled binary on a device, run "ant debug install" from the
project root directory, first setting the "debuggable" flag to "true" in the
AndroidManifest.xml file located in the root directory. To build a an unsigned
release version, run "ant release" instead.

Team Rogues:
Jesse Wihlman       - production
Ilkka Raunio        - design & art
Janette Sulin       - art
Lasse Pänkäläinen   - art
Joel Partanen       - programming
Juho Lommi          - programming
Pyry Rauhala        - programming
Juha-Matti Moilanen - programming
Keijo Pitkäniemi    - sound & music
