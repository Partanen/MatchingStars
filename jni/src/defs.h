/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <cstdint>
#include <time.h>
#include <assert.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>

typedef int             bool32;
typedef unsigned int    uint;
typedef char            byte;
typedef unsigned char   ubyte;
typedef struct IVec2    IVec2;
typedef struct FVec2    FVec2;
typedef struct UIVec2   UIVec2;
typedef struct IRect    IRect;

#define RESOLUTION_WIDTH 1080
#define RESOLUTION_HEIGHT 1920
#define MARK_PIXEL_SIZE 180
#define DROPMENU_MAX_BUTTONS 8

#define MIN(val, min)\
    (val >= min ? val : min)

#define MAX(val, max)\
    (val <= max ? val : max)

#define CLAMP(val, min, max)\
    (MIN(MAX(val, max), min))

inline IVec2
IVec2_subs(IVec2 left, IVec2 right);

static inline int
IVec2_len(IVec2 vec);

static inline int
IVec2_dotProduct(IVec2 vec1, IVec2 vec2);

static inline float
IVec2_angleBetween(IVec2 vec1, IVec2 vec2);

static inline FVec2
FVec2_add(FVec2 left, FVec2 right);

static inline FVec2
FVec2_subs(FVec2 left, FVec2 right);

inline bool32
IRect_containsPoint(IRect *rect, int x, int y);

inline bool32
IRect_isColliding(IRect *rect1, IRect *rect2);

struct UIVec2
{
    uint x, y;
};

struct IVec2
{
    int x, y;
};

struct FVec2
{
    float x, y;
};

struct IRect
{
    int x, y, w, h;
};

inline IVec2
IVec2_subs(IVec2 left, IVec2 right)
{
    IVec2 ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

static inline int
IVec2_len(IVec2 vec)
{
    return (int)sqrtf((float)(vec.x * vec.x + vec.y * vec.y));
}
static inline FVec2
FVec2_add(FVec2 left, FVec2 right)
{
    FVec2 ret = { left.x + right.x, left.y + right.y };
    return ret;
}
static inline FVec2
FVec2_subs(FVec2 left, FVec2 right)
{
    FVec2 ret = { left.x - right.x, left.y - right.y };
    return ret;
}
static inline int
IVec2_dotProduct(IVec2 vec1, IVec2 vec2)
{
    return vec1.x * vec2.x + vec1.y * vec2.y;
}
static inline float
IVec2_angleBetween(IVec2 vec1, IVec2 vec2)
{
    return acos((float)IVec2_dotProduct(vec1, vec2) / (IVec2_len(vec1) * IVec2_len(vec2)));
}

inline char *
Convert_IntToString(char *buffer, int bufferlength,  int index)
{
    snprintf(buffer, bufferlength, "%i", index);
    return buffer;
}

inline bool32
IRect_containsPoint(IRect *rect, int x, int y)
{
    return (x >= rect->x && x <= rect->x + rect->w
        &&  y >= rect->y && y <= rect->y + rect->h);
}

inline bool32
IRect_inRangeOfX(IRect *rect, int x)
{
    return x >= rect->x && x <= rect->x + rect->w;
}

inline bool32
IRect_inRangeOfY(IRect *rect, int y)
{
    return y >= rect->y && y <= rect->y + rect->h;
}

inline bool32
IRect_isColliding(IRect *rect1, IRect *rect2)
{
    return rect1->x <= rect2->x + rect2->w
        && rect1->y <= rect2->y + rect2->h
        && rect1->x >= rect2->x - rect1->w
        && rect1->y >= rect2->y - rect1->h;
}

#ifdef _DEBUG
    #include <SDL.h>
    #define DEBUG_PRINTF(str, ...)\
        SDL_Log(str, ##__VA_ARGS__)
    #define SET_BREAKPOINT()\
        raise(SIGINT)
#else
    #define DEBUG_PRINTF(str, ...)
    #define SET_BREAKPOINT()
#endif

/* A hack fix for some dlfree errors */
extern pthread_mutex_t GLOBAL_MALLOC_MUTEX;

static inline void
lockGlobalMallocMutex()
{
    pthread_mutex_lock(&GLOBAL_MALLOC_MUTEX);
}

static inline void
unlockGlobalMallocMutex()
{
    pthread_mutex_unlock(&GLOBAL_MALLOC_MUTEX);
}
