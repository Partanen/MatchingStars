/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include "core.h"
/*
static void
DebugScreen_menu_button_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_MAIN_MENU);
}

//game->current_screen = SCREEN_ACTIVE_GAME;

static void
DebugScreen_start_button_func(Button *button, Game *game)
{
    if (game->debug_screen.ui.state == MENU_STATE_NORMAL)
    {
        game->debug_screen.ui.state = MENU_STATE_MISSIONS;
    }
}
*/
static void
DebugScreen_dropmenu_button_func(Button *button, Game *game)
{
    if (game->debug_screen.ui.state == MENU_STATE_NORMAL)
    {
        game->debug_screen.ui.state = MENU_STATE_PAUSE;
    }
    else if (game->debug_screen.ui.state == MENU_STATE_PAUSE)
    {
        game->debug_screen.ui.state = MENU_STATE_NORMAL;
    }
}

static void
DebugScreen_Mission1_selection(Button *button, Game *game)
{
    if (game->debug_screen.ui.state == MENU_STATE_MISSIONS && button->blocked != 1)
    {
        game->debug_screen.ui.prev_state = game->debug_screen.ui.state;
        game->debug_screen.ui.state = MENU_STATE_BATTLES1;
    }
}

static void
DebugScreen_Back_func(Button *button, Game *game)
{
    game->debug_screen.ui.state = game->debug_screen.ui.prev_state;
}

static void
DebugScreen_DropMenu_quit_func(Button *button, Game *game)
{
    game->debug_screen.ui.prev_state = game->debug_screen.ui.state;
    game->debug_screen.ui.state = MENU_STATE_QUIT_CONFIRM;
}

static void
DebugScreen_Quit_confirm_func(Button *button, Game *game)
{
    Game_shutDown(game);
}

static void
DebugScreen_Game_start(Button *button, Game *game)
{
    if (button->blocked != 1)
        Game_setScreen(game, SCREEN_ACTIVE_GAME);
}

static void 
DebugScreen_Gameover_quit_func(Button *button, Game *game)
{
    game->debug_screen.ui.state = MENU_STATE_BATTLES1;
}

static void 
DebugScreen_Gameover_tryagain_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_ACTIVE_GAME);
}
/*
static void
DebugScreen_Options_func(Button *button, Game *game)
{
    game->debug_screen.ui.prev_state = game->debug_screen.ui.state;
    game->debug_screen.ui.state = MENU_STATE_OPTIONS;
}
*/
static void
DebugScreen_Volume_slider_press(Button *button, Game *game)
{
    button->slider = 1;
}

static void
DebugScreen_Volume_slider_release(Button *button, Game *game)
{
    button->slider = 0;
}

int
DebugScreen_init(DebugScreen *screen, Game *game)
{
    screen->ui = {screen->ui.state = MENU_STATE_NORMAL};
    screen->ui.game = game;
    
    DropMenu_create(&screen->ui,
                    { 0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT},
                     50, 50, 0,Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_NORMAL);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    100, 200, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_PAUSE);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 130, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_MISSIONS);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_BATTLES1);
                    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 100, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_QUIT_CONFIRM);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    100, 300, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_GAMEOVER);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 50, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_OPTIONS);
    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 50, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_VICTORY);
    
    /*
    DropMenu_addButton(&screen->ui.menus[0], 0, DebugScreen_menu_button_func, Assets_getTexture(&game->assets, TEX_MAIN_BUTTON_PLACEHOLDER), 0);
    DropMenu_addButton(&screen->ui.menus[0], 0, DebugScreen_start_button_func, Assets_getTexture(&game->assets, TEX_BUTTON_PLACEHOLDER1), 0);
    DropMenu_addButton(&screen->ui.menus[0], 0, DebugScreen_dropmenu_button_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    */
    
    DropMenu_addButton(&screen->ui.menus[1], 0, DebugScreen_dropmenu_button_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[1], 0, DebugScreen_DropMenu_quit_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[2], 0, DebugScreen_Mission1_selection, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[2], 0, 0, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[2], 0, 0, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, DebugScreen_Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[4], 0, DebugScreen_Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[4], 0, DebugScreen_Quit_confirm_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[4], 0, 0, 0, "Quit Battle?");
    

    DropMenu_addButton(&screen->ui.menus[5], 0, DebugScreen_Gameover_tryagain_func, 0, "Try Again");
    DropMenu_addButton(&screen->ui.menus[5], 0, DebugScreen_Gameover_quit_func, 0, "Quit");
    
    DropMenu_addButton(&screen->ui.menus[6], 0, DebugScreen_Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[6], 0, 0, 0, "Volume");
    DropMenu_addButton(&screen->ui.menus[6], 0, 0, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[6], DebugScreen_Volume_slider_press, DebugScreen_Volume_slider_release, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[7], 0, 0, 0, "Rewards");
    DropMenu_addButton(&screen->ui.menus[7], 0, 0, 0, "Points: 420 000");
    DropMenu_addButton(&screen->ui.menus[7], 0, 0, 0, "Rank: S");
    DropMenu_addButton(&screen->ui.menus[7], 0, 0, 0, "Next");
    
    return 0;
}

void
DebugScreen_onSetCurrent(DebugScreen *screen, Game *game)
{
}

void
DebugScreen_render (DebugScreen *screen, Game *game, int *viewport)
{
    glViewport(viewport[0], viewport[1], game->window.width, game->window.height);
    glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
    glClear(GL_COLOR_BUFFER_BIT);

    UI_draw(&screen->ui, game);

    SpriteBatch_flush(&game->spritebatch, viewport);
    SDL_GL_SwapWindow(game->window.sdl_window);
}

void
DebugScreen_update (DebugScreen *screen, Game *game)
{
    UI_update(&screen->ui, game);
}

void
DebugScreen_handleEvent(DebugScreen *screen, Game *game, SDL_Event *event)
{
    switch (event->type)
    {
        case SDL_FINGERDOWN:
            break;
        case SDL_FINGERUP:
            break;
        case SDL_KEYDOWN:
        {
            if (event->key.keysym.sym == SDLK_AC_BACK)
            {
                Game_setScreen(game, SCREEN_MAIN_MENU);
                game->debug_screen.ui.state = MENU_STATE_NORMAL;
            }
        }
            break;
    }
}
