/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "spritebatch.h"
#include "defs.h"

#define MAX_VISUAL_EFFECTS 64
#define MAX_TEXT_EFFECTS 16

/* Forward declaration(s) */
struct RenderThread;

int
RenderThread_enqueueTextureJob(RenderThread *thread,
    Texture *texture, SDL_Surface *surface);

typedef struct TextureArea              TextureArea;
typedef struct AnimationFrame           AnimationFrame;
typedef struct Animation                Animation;
typedef struct AnimatedObject           AnimatedObject;
typedef struct TextureArea              TextureArea;
typedef struct OnDemandTexture          OnDemandTexture;
typedef struct VisualEffectType         VisualEffectType;
typedef struct VisualEffect             VisualEffect;
typedef struct QueuedVisualEffect       QueuedVisualEffect;
typedef struct VisualEffectContainer    VisualEffectContainer;
typedef struct TextSprite               TextSprite;
typedef struct TextSpriteContainer      TextSpriteContainer;

enum AnimationState
{
    ANIMATION_STATE_PLAYING,
    ANIMATION_STATE_PAUSED,
    ANIMATION_STATE_STOPPED
};

void
loadTextureAndFreePrevious(Texture *texture,
     void *pixels, unsigned int w, unsigned int h,
     GLenum color_format, GLenum wrap_mode);

void
freeTexture(Texture *texture);

/* This must be called from the render thread */
/* Return 0 on success */
int
loadTextureFromFile(Texture *texture, const char *path);

/* This must be called from the render thread */
/* Return 0 on success */
int
loadTextureFromText(Texture *texture, const char *text,
    TTF_Font *font, SDL_Color color,
    unsigned int wrap_width);

/* Load a texture from text - can be called from any thread. The surface is created
 * on the calling thread, so the width and height are known immediately.
 * If an old OpenGL texture, it will be freed first.
 * Return 0 on successful surface creation */
int
loadTextureFromTextOnAnyThread(RenderThread *render_thread, Texture *texture,
    const char *text, TTF_Font *font, SDL_Color color, unsigned int wrap_width);


/* Load a texture from a bitmap file - can be called from any thread.
 * The surface is created on the calling thread, so the width and height are known immediately.
 * If an old OpenGL texture, it will be freed first.
 * Return 0 on successful surface creation */
int
loadTextureFromFileOnAnyThread(RenderThread *render_thread,
    Texture *texture, const char *path);

inline void
AnimationFrame_create(AnimationFrame *frame,
    Texture *texture,
    uint clip_x, uint clip_y, uint clip_w, uint clip_h,
    float dur,
    int offset_x, int offset_y,
    SpriteFlip flip,
    float angle,
    float scale_x, float scale_y);

void
Animation_create(Animation *animation, AnimationFrame *frames, uint numframes);

void
Animation_free(Animation *animation);

void
Animation_createFromSheet(Animation *animation, Texture *texture,
    uint frame_width, uint frame_height, float frame_duration,
    uint start_x, uint start_y, uint num_frames, uint sprites_in_row);

void
Animation_createFromSinglePicture(Animation *animation, Texture *texture,
    uint frame_width, uint frame_height, float frame_duration,
    uint start_x, uint start_y, uint num_frames);

/* AnimatedObjects */
inline AnimatedObject
AnimatedObject_create(Animation *animation, bool32 loop, bool32 start_playing, float speed);

void
AnimatedObject_update(AnimatedObject *object, double delta);

void
AnimatedObject_draw(AnimatedObject *object, SpriteBatch *batch, int x, int y);

void
AnimatedObject_draw_Flip(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip);

void
AnimatedObject_draw_Scale(AnimatedObject *object, SpriteBatch *batch, int x, int y,
    float scale_x, float scale_y);

void
AnimatedObject_draw_Rotate(AnimatedObject *object, SpriteBatch *batch, int x, int y);

void
AnimatedObject_draw_FlipScale(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip, float scale_x, float scale_y);

void
AnimatedObject_draw_FlipScaleRotate(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip, float scale_x, float scale_y, float angle);

void
AnimatedObject_draw_ClipPercentage(AnimatedObject *object,
    SpriteBatch *batch, int x, int y,
    float percent_x, float percent_y);

inline void
AnimatedObject_playAnimation(AnimatedObject *object,
    Animation *animation,
    bool32 loop, float speed);

inline void
AnimatedObject_replay(AnimatedObject *object);

inline void
AnimatedObject_pause(AnimatedObject *object);

inline void
AnimatedObject_resume(AnimatedObject *object);

inline void
AnimatedObject_stop(AnimatedObject *object);

inline bool32
AnimatedObject_isFinished(AnimatedObject *object);

inline VisualEffectType
VisualEffectType_create(Animation *animation, void (*onUpdate)(VisualEffect *effect, void*));

void
VisualEffectContainer_init(VisualEffectContainer *container);

/* Return non-zero if no free slots are found */
int
playVisualEffect(VisualEffectContainer *container,
    VisualEffectType *type,
    void* update_args,
    SpriteFlip flip, float rot,
    float scale_x, float scale_y,
    float animation_speed,
    int start_x, int start_y,
    int end_x, int end_y);

int
queueVisualEffect(VisualEffectContainer *container,
    VisualEffectType *type,
    void* update_args,
    SpriteFlip flip, float rot,
    float scale_x, float scale_y,
    float animation_speed,
    int start_x, int start_y,
    int end_x, int end_y);

void
VisualEffectContainer_update(VisualEffectContainer *container, double dt);

void
VisualEffectContainer_render(VisualEffectContainer *container, SpriteBatch *batch);

void
TextSpriteContainer_init(TextSpriteContainer *container);

TextSprite*
TextSprite_reserve(TextSpriteContainer *container, RenderThread *thread, const char* text, TTF_Font *font, SDL_Color color);

void
TextSprite_release(TextSpriteContainer *container, double dt);

struct TextureArea
{
    Texture *texture;
    uint    clip[4];
};

struct AnimationFrame
{
    Texture     *texture;
    uint        clip[4];
    float       duration; /* In seconds */
    IVec2       offset;
    SpriteFlip  flip;
    float       angle;
    FVec2       scale;
};

struct Animation
{
    AnimationFrame  *frames;
    int             num_frames;
    float           total_duration; /* In seconds */
};

struct AnimatedObject
{
    Animation       *animation;
    int             frame;
    float           time_passed;
    float           speed;
    AnimationState  state;
    bool32          loop;
};

/* A texture that gets unloaded automatically when not used,
 * but whose bitmap remains in memory throughout the lifespan
 * of the application */
struct OnDemandTexture
{
    bool32      loading;
    uint        last_use_time;
    SDL_Surface *bitmap;
    Texture     texture;
};

struct VisualEffectType
{
    Animation       *animation;
    void            (*onUpdate)(VisualEffect *effect, void*);
};

struct VisualEffect
{
    VisualEffectType    *type;
    AnimatedObject      aobject;
    IVec2               start_pos;
    IVec2               end_pos;
    IVec2               pos;
    VisualEffect        *next;
    VisualEffect        *prev;
    void                *update_args;
    float               duration;
    float               time_passed;
    bool32              visualdone;
    float               rot;
    FVec2               scale;
    SpriteFlip          flip;
};

struct QueuedVisualEffect
{
    VisualEffectType    *type;
    IVec2               start;
    IVec2               end;
    void                *update_args;
    float               rot;
    FVec2               scale;
    SpriteFlip          flip;
    float               speed;
};

struct VisualEffectContainer
{
    VisualEffect        effects[MAX_VISUAL_EFFECTS];
    QueuedVisualEffect  queue[MAX_VISUAL_EFFECTS];
    int                 num_queued;
    VisualEffect        *active_effects;
    VisualEffect        *free_effects;
    pthread_mutex_t     mutex;
    pthread_mutex_t     queue_mutex;
};

struct TextSprite
{
    Animation           animation;
    Texture             texture;
    TextSprite          *next;
    TextSprite          *prev;
    VisualEffectType    numberef;
    void                *update_args;
    float               duration;
    float               time_passed;
};

struct TextSpriteContainer
{
    TextSprite          textsprites[MAX_TEXT_EFFECTS];
    TextSprite          *active_textsprites;
    TextSprite          *free_textsprites;

};

inline void
AnimationFrame_create(AnimationFrame *frame,
    Texture *texture,
    uint clip_x, uint clip_y, uint clip_w, uint clip_h,
    float dur,
    int offset_x, int offset_y,
    SpriteFlip flip,
    float angle,
    float scale_x, float scale_y)
{
    frame->texture = texture;
    frame->clip[0] = clip_x;
    frame->clip[1] = clip_y;
    frame->clip[2] = clip_w;
    frame->clip[3] = clip_h;
    frame->duration = dur;
    frame->offset.x = offset_x;
    frame->offset.y = offset_y;
    frame->flip = flip;
    frame->angle = angle;
    frame->scale.x = scale_x;
    frame->scale.y = scale_y;
}

inline AnimatedObject
AnimatedObject_create(Animation *animation, bool32 loop, bool32 start_playing, float speed)
{
    AnimatedObject aobject;
    aobject.animation   = animation;
    aobject.frame       = 0;
    aobject.time_passed = 0;
    aobject.speed       = speed;
    aobject.state       = start_playing ? ANIMATION_STATE_PLAYING : ANIMATION_STATE_STOPPED;
    aobject.loop        = loop;
    return aobject;
}

inline void
AnimatedObject_playAnimation(AnimatedObject *object, Animation *animation, bool32 loop, float speed)
{
    object->animation   = animation;
    object->frame       = 0;
    object->time_passed = 0;
    object->speed       = speed;
	object->state		= ANIMATION_STATE_PLAYING;
    object->loop        = loop;
}

inline void
AnimatedObject_replay(AnimatedObject *object)
{
    object->time_passed = 0.0f;
    object->frame = 0;
    object->state = ANIMATION_STATE_PLAYING;
}

inline void
AnimatedObject_pause(AnimatedObject *object)
{
    object->state = ANIMATION_STATE_PAUSED;
}

inline void
AnimatedObject_resume(AnimatedObject *object)
{
    object->state = ANIMATION_STATE_PLAYING;
}

inline void
AnimatedObject_stop(AnimatedObject *object)
{
    object->state = ANIMATION_STATE_STOPPED;
}

inline bool32
AnimatedObject_isFinished(AnimatedObject *object)
{
    if (!object->animation)
        return 1;

    return object->frame >= object->animation->num_frames - 1
    && object->time_passed >= object->animation->frames[object->frame].duration;
}

inline VisualEffectType
VisualEffectType_create(Animation *animation, void (*onUpdate)(VisualEffect *effect, void*))
{
    VisualEffectType effect;
    effect.animation    = animation;
    effect.onUpdate     = onUpdate;
    return effect;
}

void
TextSprite_init(TextSprite *textsprite);

void
TextSprite_setText(RenderThread *thread,
    TextSprite *textsprite, const char* text,
    TTF_Font *font, SDL_Color color);

void
TextSprite_draw(SpriteBatch *batch, Texture *texture, int x, int y, int widthscale, int heightscale);

void
freeSurfaceJob(void *); /* For freeing SDL surfaces on the main thread */
