/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */


#include "core.h"
/*
static void
Menu_button_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_MAIN_MENU);
}

static void
Start_button_func(Button *button, Game *game)
{
    if (game->active_game_screen.ui.state == MENU_STATE_NORMAL)
    {
        game->active_game_screen.ui.state = MENU_STATE_MISSIONS;
    }
}
*/
static void
Pause_button_func(Button *button, Game *game)
{
    if (game->active_game_screen.ui.state == MENU_STATE_NORMAL && button->blocked != 1)
    {
        game->active_game_screen.ui.state = MENU_STATE_PAUSE;
        game->active_game_screen.game_paused = true;
    }
    else if (game->active_game_screen.ui.state == MENU_STATE_PAUSE && button->blocked != 1)
    {
        game->active_game_screen.ui.state = MENU_STATE_NORMAL;
        game->active_game_screen.game_paused = false;
    }
}

static void
Mission1_selection(Button *button, Game *game)
{
    if (game->active_game_screen.ui.state == MENU_STATE_MISSIONS && button->blocked != 1 && button->mission_number == 1)
    {
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        game->active_game_screen.ui.state = MENU_STATE_BATTLES1;
    }
    else if (game->active_game_screen.ui.state == MENU_STATE_MISSIONS && button->blocked != 1 && button->mission_number == 2)
    {
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        game->active_game_screen.ui.state = MENU_STATE_BATTLES2;
    }
    else if (game->active_game_screen.ui.state == MENU_STATE_MISSIONS && button->blocked != 1 && button->mission_number == 3)
    {
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        game->active_game_screen.ui.state = MENU_STATE_BATTLES3;
    }
}

static void
Back_func(Button *button, Game *game)
{
        game->active_game_screen.ui.state = game->active_game_screen.ui.prev_state;
}

static void
Error_func(Button *button, Game *game)
{
        game->active_game_screen.ui.state = MENU_STATE_VICTORY;
}
/*
static void
Quit_func(Button *button, Game *game)
{
    game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
    game->active_game_screen.ui.state = MENU_STATE_QUIT_CONFIRM;
}
*/
static void
Quit_confirm_func(Button *button, Game *game)
{
    Game_shutDown(game);
}

static void
Game_start(Button *button, Game *game)
{
    if (button->mission_number == 1 && button->battle_number == 1)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 2 && game->active_game_screen.ui.buttons[i].mission_number == 1)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[0].missions[MISSION_1_INDEX], game);
    }
    else if (button->mission_number == 1 && button->battle_number == 2 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 1)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 3 && game->active_game_screen.ui.buttons[i].mission_number == 1)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[0].missions[1], game);
    }
    else if (button->mission_number == 1 && button->battle_number == 3 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 2)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 4 && game->active_game_screen.ui.buttons[i].mission_number == 1)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[0].missions[2], game);
    }
    else if (button->mission_number == 1 && button->battle_number == 4 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 3)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 8055 && game->active_game_screen.ui.buttons[i].mission_number == 1)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[0].missions[3], game);
    }
    else if (button->mission_number == 1 && button->battle_number == 8055 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 4)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].mission_number == 2 && game->active_game_screen.ui.buttons[i].battle_number == 0)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[0].missions[4], game);
    }
    else if (button->mission_number == 2 && button->battle_number == 1 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 6)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 2 && game->active_game_screen.ui.buttons[i].mission_number == 2)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[1].missions[0], game);
    }
    else if (button->mission_number == 2 && button->battle_number == 2 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 7)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 3 && game->active_game_screen.ui.buttons[i].mission_number == 2)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[1].missions[1], game);
    }
    else if (button->mission_number == 2 && button->battle_number == 3 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 8)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 4 && game->active_game_screen.ui.buttons[i].mission_number == 2)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[1].missions[2], game);
    }
    else if (button->mission_number == 2 && button->battle_number == 4 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 9)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 8055 && game->active_game_screen.ui.buttons[i].mission_number == 2)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[1].missions[3], game);
    }
    else if (button->mission_number == 2 && button->battle_number == 8055 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 10)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].mission_number == 3 && game->active_game_screen.ui.buttons[i].battle_number == 0)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[1].missions[4], game);
    }
    else if (button->mission_number == 3 && button->battle_number == 1 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 12)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 2 && game->active_game_screen.ui.buttons[i].mission_number == 3)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[2].missions[0], game);
    }
    else if (button->mission_number == 3 && button->battle_number == 2 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 13)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 3 && game->active_game_screen.ui.buttons[i].mission_number == 3)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[2].missions[1], game);
    }
    else if (button->mission_number == 3 && button->battle_number == 3 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 14)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 4 && game->active_game_screen.ui.buttons[i].mission_number == 3)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[2].missions[2], game);
    }
    else if (button->mission_number == 3 && button->battle_number == 4 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 15)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 8055 && game->active_game_screen.ui.buttons[i].mission_number == 3)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[2].missions[3], game);
    }
    else if (button->mission_number == 3 && button->battle_number == 8055 && game->active_game_screen.session.savefiles.num_missions_unlocked >= 16)
    {
        for (int i = 0; i < game->active_game_screen.ui.button_amount; i++)
        {
            if (game->active_game_screen.ui.buttons[i].battle_number == 8055 && game->active_game_screen.ui.buttons[i].mission_number == 3)
            {
                game->active_game_screen.ui.victory_unlock = &game->active_game_screen.ui.buttons[i];
            }
        }
        game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
        GameSession_beginMission(&game->active_game_screen.session,
            game->game_data.level_types[2].missions[4], game);
    }
    
    game->active_game_screen.ui.buttons[0].blocked = 0;
}

static void
Gameover_quit_func(Button *button, Game *game)
{
    game->active_game_screen.game_paused = false;
    game->active_game_screen.ui.state = game->active_game_screen.ui.prev_state;
    game->active_game_screen.ui.prev_state = MENU_STATE_MISSIONS;
    game->active_game_screen.session.state = SESSION_STATE_MISSION_SELECTION;
    playMissionSelectionMusic(game);
}

static void
Gameover_tryagain_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_ACTIVE_GAME);
}
/*
static void
Options_func(Button *button, Game *game)
{
    game->active_game_screen.ui.prev_state = game->active_game_screen.ui.state;
    game->active_game_screen.ui.state = MENU_STATE_OPTIONS;
}
*/
static void
Volume_slider_press(Button *button, Game *game)
{
    button->slider = 1;
}

static void
Volume_slider_release(Button *button, Game *game)
{
    button->slider = 0;
}

int
ActiveGameScreen_init(ActiveGameScreen *screen, Game *game)
{
    screen->ui = {screen->ui.state = MENU_STATE_MISSIONS};
    screen->game_paused = false;
    screen->ui.game = game;

    DropMenu_create(&screen->ui,
                    { 0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT},
                     50, 50, 0, Assets_getTexture(&game->assets, TEX_GAMEBOARD), MENU_STATE_NORMAL);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    100, 200, Assets_getTexture(&game->assets, TEX_GAMEBOARD), Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_PAUSE);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 130, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_MISSIONS);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 100, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_QUIT_CONFIRM);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    100, 300, 0,Assets_getTexture(&game->assets, TEX_MATLOCK),
                    MENU_STATE_GAMEOVER);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 50, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_OPTIONS);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 50, 0,Assets_getTexture(&game->assets, TEX_MATLOCK),
                        MENU_STATE_VICTORY);
                    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_BATTLES1);
                    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_BATTLES2);

    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_BATTLES3);
                    
    DropMenu_create(&screen->ui,
                    {GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780},
                    50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD),Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_ERROR);
    
    DropMenu_addButton(&screen->ui.menus[2], 0, Mission1_selection, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[7], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[7], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[7], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[7], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[7], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[2], 0, Mission1_selection, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[2], 0, Mission1_selection, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Game_start, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[0], 0, Pause_button_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    /*
    DropMenu_addButton(&screen->ui.menus[0], 0, Menu_button_func, Assets_getTexture(&game->assets, TEX_MAIN_BUTTON_PLACEHOLDER), 0);
    DropMenu_addButton(&screen->ui.menus[0], 0, Start_button_func, Assets_getTexture(&game->assets, TEX_BUTTON_PLACEHOLDER1), 0);
    */
    DropMenu_addButton(&screen->ui.menus[1], 0, Pause_button_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[1], 0, Gameover_quit_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);

    DropMenu_addButton(&screen->ui.menus[3], 0, Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, Quit_confirm_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[3], 0, 0, 0, "Quit Battle?");

    DropMenu_addButton(&screen->ui.menus[4], 0, Gameover_tryagain_func, 0, "Try Again");
    DropMenu_addButton(&screen->ui.menus[4], 0, Gameover_quit_func, 0, "Quit");

    DropMenu_addButton(&screen->ui.menus[5], 0, Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[5], 0, 0, 0, "Volume");
    DropMenu_addButton(&screen->ui.menus[5], 0, 0, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[5], Volume_slider_press, Volume_slider_release, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);

    //DropMenu_addButton(&screen->ui.menus[6], 0, 0, 0, "Rewards");
    DropMenu_addButton(&screen->ui.menus[6], 0, 0, 0, 0);
    //DropMenu_addButton(&screen->ui.menus[6], 0, 0, 0, "Rank: S");
    DropMenu_addButton(&screen->ui.menus[6], 0, Gameover_quit_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[7], 0, Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[8], 0, Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    DropMenu_addButton(&screen->ui.menus[9], 0, Back_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[10], 0, 0, 0, "Could not");
    DropMenu_addButton(&screen->ui.menus[10], 0, Error_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);
    
    DropMenu_addButton(&screen->ui.menus[2], 0, setFps30, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), "30fps");
    DropMenu_addButton(&screen->ui.menus[2], 0, setFps60, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), "60fps");


    screen->texar_mark_repair       = {Assets_getTexture(&game->assets, TEX_MARKS),
        {0, 3*79, 79, 79}};
    screen->texar_mark_shield       = {Assets_getTexture(&game->assets, TEX_MARKS),
        {0, 4*79, 79, 79}};
    screen->texar_mark_ballistic    = {Assets_getTexture(&game->assets, TEX_MARKS),
        {0, 0, 79, 79}};
    screen->texar_mark_rocket       = {Assets_getTexture(&game->assets, TEX_MARKS),
        {0, 2*79, 79, 79}};
    screen->texar_mark_laser        = {Assets_getTexture(&game->assets, TEX_MARKS),
        {0, 79, 79, 79}};

    //SDL_Color white = {255, 255, 255};

    /*
    loadTextureFromTextOnAnyThread(&game->render_thread, &screen->tex_mission_selection_header,
        "Mission selection", Assets_getFont(&game->assets, FONT_ROBOTO_50),
        white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread, &screen->tex_mission_selection_text,
        "Touch the screen to start a mission", Assets_getFont(&game->assets, FONT_ROBOTO_50),
        white, 0);
    */

    GameSession_initializeNew(&screen->session, game);

	screen->bg_x = 0;
	screen->bg_y = 0;
	screen->scroll_value = 1;

    screen->ao_player_ship = AnimatedObject_create(&game->assets.animation1, 1, 1, 1.0f);

    return 0;
}

void
ActiveGameScreen_onSetCurrent(ActiveGameScreen *screen, Game *game)
{
}

void
ActiveGameScreen_render(ActiveGameScreen *screen, Game *game, int *viewport)
{
	GameSession_render(screen, &screen->session, game, viewport);
    UI_draw(&screen->ui, game);

    if (game->active_game_screen.ui.state == MENU_STATE_BATTLES1)
    {
        SpriteBatch_drawSprite(&game->spritebatch,
                              Assets_getTexture(&game->assets, TEX_MISSION1_MENU), (RESOLUTION_WIDTH - 901) / 2, 140, 0);
    }
    else if (game->active_game_screen.ui.state == MENU_STATE_BATTLES2)
    {
        SpriteBatch_drawSprite(&game->spritebatch,
                              Assets_getTexture(&game->assets, TEX_MISSION2_MENU), (RESOLUTION_WIDTH - 901) / 2, 140, 0);
    }
    else if (game->active_game_screen.ui.state == MENU_STATE_BATTLES3)
    {
        SpriteBatch_drawSprite(&game->spritebatch,
                              Assets_getTexture(&game->assets, TEX_MISSION3_MENU), (RESOLUTION_WIDTH - 901) / 2, 140, 0);
    }

    if (screen->ui.state == MENU_STATE_MISSIONS)
    {
        AnimatedObject_draw_Scale(&screen->ao_player_ship, &game->spritebatch,
            RESOLUTION_WIDTH / 2 - (int)(2.0f * 0.4f * (float)game->assets.animation1.frames->clip[2]) / 2,
            275, 2.0f, 2.0f);
    }

    SpriteBatch_flush(&game->spritebatch, game->window.viewport);
    SDL_GL_SwapWindow(game->window.sdl_window);
}

void
ActiveGameScreen_update(ActiveGameScreen *screen, Game *game)
{
	GameSession_update(&screen->session, game);
    AnimatedObject_update(&screen->ao_player_ship, game->clock.delta);
    UI_update(&screen->ui, game);

	//Background scroll
	screen->bg_x += screen->scroll_value;
	if(screen->bg_x > RESOLUTION_WIDTH)
		screen->bg_x = 0;

#ifdef _DEBUG
    updateFpsTextTexture(&game->assets.fpsText,
        &game->render_thread,
        game->clock.fps,
        Assets_getFont(&game->assets, FONT_ELECTROLIZE));
#endif
}

void
ActiveGameScreen_handleEvent(ActiveGameScreen *screen, Game *game, SDL_Event *event)
{
    switch (event->type)
    {
        case SDL_KEYDOWN:
        {
            if (event->key.keysym.sym == SDLK_AC_BACK)
            {
                #ifdef _DEBUG
                    game->debug_screen.ui.state = MENU_STATE_NORMAL;
                #endif

                game->menu_screen.start_animation.frame = 0;
                Game_setScreen(game, SCREEN_MAIN_MENU);
            }
        }
        break;
    }
}
