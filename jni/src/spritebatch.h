/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <GLES2/gl2.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef enum SpriteFlip
{
     SPRITE_FLIP_NONE,
     SPRITE_FLIP_HORIZONTAL,
     SPRITE_FLIP_VERTICAL,
     SPRITE_FLIP_BOTH
} SpriteFlip;

typedef struct Texture              Texture;
typedef struct TextureBufferItem    TextureBufferItem;
typedef struct SpriteBatch          SpriteBatch;

/* Return 0 on success */
int
SpriteBatch_init(SpriteBatch *batch, GLchar *error_log, GLint log_len);

void
SpriteBatch_dispose(SpriteBatch *batch);

/* Note: setting clip to 0 is allowed to draw a full texture */
void
SpriteBatch_drawSprite(SpriteBatch *batch, Texture *texture,
                       int x, int y, unsigned int *clip);

void
SpriteBatch_drawSprite_Flip(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip);

void
SpriteBatch_drawSprite_Scale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_Rotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float angle);

void
SpriteBatch_drawSprite_FlipScale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_FlipRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip, float angle);

void
SpriteBatch_drawSprite_ScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y, float angle);

void
SpriteBatch_drawSprite_FlipScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip,
    SpriteFlip flip, float scale_x, float scale_y, float angle);

void
SpriteBatch_flush(SpriteBatch *batch, int *viewport);

struct Texture
{
    GLuint          id;
    unsigned int    width;
    unsigned int    height;
};

struct TextureBufferItem
{
    GLuint id;
    unsigned int repeats;
};

struct SpriteBatch
{
    GLuint              program;
    GLuint              vbo;
    GLuint              ebo;
    GLint               proj_loc;

    GLfloat             *vertex_buffer;
    TextureBufferItem   *texture_buffer;
    unsigned int        buffer_size;

    unsigned int        sprite_count;
    unsigned int        tex_swap_count;

    GLfloat             scale_x;
    GLfloat             scale_y;
};

