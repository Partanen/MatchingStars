/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <SDL_ttf.h>
#include "render.h"

#define NUM_FONT_ASSETS (sizeof(FONT_ASSETS) / sizeof(FontAsset))
#define NUM_TEXTURE_ASSETS (sizeof(TEXTURE_PATHS) / sizeof(const char*))
#define NUM_MUSIC_ASSETS (sizeof(MUSIC_PATHS) / sizeof(const char*))
#define NUM_SOUND_CLIPS (sizeof(SOUND_CLIP_PATHS) / sizeof(const char*))
#define NUM_ON_DEMAND_TEXTURE_ASSETS (sizeof(ON_DEMAND_TEXTURE_PATHS) / sizeof(const char*))

typedef struct FontAsset        FontAsset;

struct FontAsset
{
    const char  *path;
    int         size;
};

FontAsset FONT_ASSETS[]
{
    #define FONT_ELECTROLIZE 0
    {"electrolize.ttf", 24}
};

/* This texture is loaded if a bitmap isn't found */
const char *PLACEHOLDER_TEXTURE_PATH = "placeholder.png";

const char *TEXTURE_PATHS[]
{
    #define TEX_MENU_BACKGROUND 0
    "Menu_Background.png",

    #define TEX_MARKS 1
    "mark_animation_sheet.png",

    #define TEX_PROGRESS_BAR1_PH 2
    "HealthBar_Spritesheet_Bigger.png",

    #define TEX_FIRST_SELECTION_INDICATOR 3
    "first_selection_indicator.png",

	#define TEX_CAPITAL_SHIP 4
	"capital_class_ship.png",

	#define TEX_LASER 5
	"Beam Spritesheet.png",

	#define TEX_EXPLOSION 6
	"ExplosionFrames.png",

	#define TEX_TARGET 7
	"crosshair.png",

    #define TEX_ROCKET 8
    "missilesSheet.png",

    #define TEX_BULLET 9
    "BallisticProjectiles.png",

    #define TEX_MARAUDERS 10
    "marauders.png",

    #define TEX_BUTTONSHEET 11
    "button_spritesheet.png",

    #define TEX_GAMEBOARD 12
    "gameboard.png",

    #define TEX_VICTORY_ANIMATION 13
    "Victory_Spritesheet.png",

    #define TEX_GAMEOVER_ANIMATION 14
    "Defeat_Spritesheet.png",

    #define TEX_MATLOCK 15
    PLACEHOLDER_TEXTURE_PATH,

    #define TEX_SHOCKWAVE 16
    "massdriver_shockwave.png",

    #define TEX_MISSION1_MENU 17
    "Mission1_Select_Screen.png",

    #define TEX_MISSION2_MENU 18
    "Mission2_Select_Screen.png",

    #define TEX_MISSION3_MENU 19
    "Mission3_Select_Screen.png",

    #define TEX_START_SCREEN 20
    "First_Screen_Background.png",

    #define TEX_START_SCREEN_ANIMATION 21
    "Menu_Opening_Spritesheet.png",

    #define TEX_ANTIMATTER_EXPLOSION 22
    "AntiMatterExplosion.png",

    #define TEX_LOGO 23
    "logo.png",

    #define TEX_ENEMIES2 24
    "second_enemy_ships.png",

    #define TEX_FIRST_BOSS 25
    "Boss1.png",

    #define TEX_SECOND_BOSS 26
    "Boss2.png",

    #define TEX_FINAL_BOSS 27
    "Final_Boss.png"
};

const char *MUSIC_PATHS[] =
{
    #define MUS_CONNECTIONLESS_SPACE 0
    "ConnectionlessSpace.wav",

    #define MUS_COMBINATED_SPACE1 1
    "CombinatedSpace1.wav",

    #define MUS_COMBINATED_SPACE2 2
    "CombinatedSpace2.wav",

    #define MUS_LEVEL_1_BOSS_INTRO 3
    "BossSpaceIntro.wav",

    #define MUS_LEVEL_1_BOSS 4
    "BossSpace.wav",

    #define MUS_LEVEL_2 5
    "FinalSpace.wav"
};

const char *SOUND_CLIP_PATHS[] =
{
    #define SO_BUTTON_1 0
    "ButtonSpace.wav",

    #define SO_CANCEL 1
    "CancelSpace.wav",

    #define SO_COMBO 2
    "ComboSpace.wav",

    #define SO_EXPLOSION_BIG 3
    "ExplosionBigSpace.wav",

    #define SO_EXPLOSION_SMALL 4
    "ExplosionLittleSpace.wav",

    #define SO_MARK_PRESS 5
    "SimplerPressSpace.wav",

    #define SO_LASER_SMALL 6
    "LaserLittleSpace.wav",

    #define SO_LASER_MEDIUM 7
    "LaserMedSpace.wav",

    #define SO_LASER_BIG 8
    "LaserBigSpace.wav",

    #define SO_ENEMYATTACK_SMALL 9
    "ZiumSpace.wav",

    #define SO_HEAL1 10
    "HealSpace.wav",

    #define SO_SHIELD1 11
    "ShieldSpace.wav",

    #define SO_ENEMYATTACK_LARGE 12
    "EnemyShootSpace.wav"
};

const char *ON_DEMAND_TEXTURE_PATHS[] =
{
    #define ODTEX_SPACE_BG_FROZEN 0
    "session_bg_frozen.png",

    #define ODTEX_SPACE_BG_GREEN 1
    "session_bg_green.png",

    #define ODTEX_SPACE_BG_TORN 2
    "session_bg_torn.png"
};
