/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include "UI.h"

void
Button_create(UI *ui,
              IRect rect,
              TextureArea texar_normal,
              TextureArea texar_press,
              void (*onPress)(Button*, Game*),
              void (*onRelease)(Button*, Game*),
              const char *label,
              int font_scale)
{
    Button *button         = &ui->buttons[ui->button_amount];
    button->rect           = rect;
    button->texar_normal   = texar_normal;
    button->texar_press    = texar_press;
    button->onPress        = onPress;
    button->onRelease      = onRelease;
    button->state          = BUTTON_STATE_NORMAL;
    button->anchor         = ANCHOR_TOP_LEFT;
    button->label_text     = label;
    button->font_scale     = font_scale;
    button->blocked        = 0;
    button->mission_number = 0;
    button->battle_number  = 0;
    button->next           = 0;
    ui->button_amount     += 1;
    
    if (button->label_text && button->font_scale >= 0)
    {
        loadTextureFromTextOnAnyThread(&ui->game->render_thread, &button->label, button->label_text,
            Assets_getFont(&ui->game->assets, FONT_ELECTROLIZE), {255,255,255,1}, 0);
    }
}

void
Button_draw(Button *button, Game *game)
{
    int x, y;

    switch (button->anchor)
    {
        case ANCHOR_TOP_LEFT:
        {
            x = button->rect.x;
            y = button->rect.y;
        }
            break;
        case ANCHOR_TOP_RIGHT:
        {
            x = game->window.width - button->rect.x - button->rect.w;
            y = button->rect.y;
        }
            break;
        case ANCHOR_TOP_CENTER:
        {
            x = game->window.width / 2 - button->rect.w / 2 + button->rect.x;
            y = button->rect.y;
        }
            break;

        case ANCHOR_BOTTOM_LEFT:
        {
            x = button->rect.x;
            y = game->window.height - button->rect.h - button->rect.y;
        }
            break;
        case ANCHOR_BOTTOM_RIGHT:
        {
            x = game->window.width - button->rect.w - button->rect.x;
            y = game->window.height - button->rect.h - button->rect.y;
        }
            break;
        case ANCHOR_BOTTOM_CENTER:
        {
            x = game->window.width / 2 + button->rect.w / 2 + button->rect.x;
            y = game->window.height - button->rect.h - button->rect.x;
        }
            break;

        case ANCHOR_CENTER_LEFT:
        {
            x = button->rect.x;
            y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        case ANCHOR_CENTER_RIGHT:
        {
            x = game->window.width - button->rect.w - button->rect.x;
            y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        case ANCHOR_CENTER_CENTER:
        {
            x = game->window.width / 2 - button->rect.w / 2 + button->rect.x;
            y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        default:
        {
            x = button->rect.x;
            y = button->rect.y;
        }
            break;
    }

    switch (button->state)
    {
        case BUTTON_STATE_NORMAL:
        {
            if (button->blocked == 1)
            {
                if (button->texar_blocked.texture)
                {
                    SpriteBatch_drawSprite(&game->spritebatch,
                    button->texar_blocked.texture, x, y,
                    button->texar_blocked.clip);
                }
            }
            else if (button->mission_number == -1 && game->clock.target_fps == 30)
            {
                SpriteBatch_drawSprite(&game->spritebatch,
                    button->texar_press.texture, x, y,
                    button->texar_press.clip);
            }
            else if (button->mission_number == -2 && game->clock.target_fps == 60)
            {
                SpriteBatch_drawSprite(&game->spritebatch,
                    button->texar_press.texture, x, y,
                    button->texar_press.clip);
            }
            else if (button->texar_normal.texture)
            {
                SpriteBatch_drawSprite(&game->spritebatch,
                    button->texar_normal.texture, x, y,
                    button->texar_normal.clip);
            }
        }
            break;
        case BUTTON_STATE_PRESSED:
        {
            TextureArea *texar = 0;

            if (button->blocked == 1)
                texar = &button->texar_blocked;
            else if (button->texar_press.texture)
                texar = &button->texar_press;
            else if (button->texar_normal.texture)
                texar = &button->texar_normal;

            if (texar)
            {
                SpriteBatch_drawSprite(&game->spritebatch,
                    texar->texture, x, y, texar->clip);
            }
        }
            break;
    }

}

void
Button_update(Button *button, UI *ui, Game *game)
{
    IRect rect;
    rect.w = button->rect.w;
    rect.h = button->rect.h;

    switch (button->anchor)
    {
        case ANCHOR_TOP_LEFT:
        {
            rect.x = button->rect.x;
            rect.y = button->rect.y;
        }
            break;
        case ANCHOR_TOP_RIGHT:
        {
            rect.x = game->window.width - button->rect.x - button->rect.w;
            rect.y = button->rect.y;
        }
            break;
        case ANCHOR_TOP_CENTER:
        {
            rect.x = game->window.width / 2 - button->rect.w / 2 + button->rect.x;
            rect.y = button->rect.y;
        }
            break;

        case ANCHOR_BOTTOM_LEFT:
        {
            rect.x = button->rect.x;
            rect.y = game->window.height - button->rect.h - button->rect.y;
        }
            break;
        case ANCHOR_BOTTOM_RIGHT:
        {
            rect.x = game->window.width - button->rect.w - button->rect.x;
            rect.y = game->window.height - button->rect.h - button->rect.y;
        }
            break;
        case ANCHOR_BOTTOM_CENTER:
        {
            rect.x = game->window.width / 2 + button->rect.w / 2 + button->rect.x;
            rect.y = game->window.height - button->rect.h - button->rect.x;
        }
            break;

        case ANCHOR_CENTER_LEFT:
        {
            rect.x = button->rect.x;
            rect.y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        case ANCHOR_CENTER_RIGHT:
        {
            rect.x = game->window.width - button->rect.w - button->rect.x;
            rect.y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        case ANCHOR_CENTER_CENTER:
        {
            rect.x = game->window.width / 2 - button->rect.w / 2 + button->rect.x;
            rect.y = game->window.height / 2 - button->rect.h / 2 + button->rect.y;
        }
            break;
        default:
        {
            rect.x = button->rect.x;
            rect.y = button->rect.y;
        }
            break;
    }

    switch (button->state)
    {
        case BUTTON_STATE_NORMAL:
        {
            for (int i = 0; i < NUM_TOUCH_FINGERS; ++i)
            {
                if (IRect_containsPoint(&rect, game->input.fingers[i].position.x,
                    game->input.fingers[i].position.y) && game->input.fingers[i].finger_down && ui->pressed == 0)
                {
                    ui->pressed = 1;
                    button->touch_finger = i;
                    button->state = BUTTON_STATE_PRESSED;

                    if (button->onPress && button->blocked != 1)
                    {
                        button->onPress(button, game);
                    }
                    break;
                }
             }
        }
            break;
        case BUTTON_STATE_PRESSED:
        {
            if (!game->input.fingers[button->touch_finger].finger_down)
            {
                button->state = BUTTON_STATE_NORMAL;
                if (button->onRelease && button->blocked != 1)
                {
                    /* Putting this here temporarily */
                    Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets, SO_BUTTON_1), 0);
                    button->onRelease(button, game);
                }
                ui->pressed = 0;
            }
        }
            break;
    }
    
    if (button->slider == 1)
    {
        if ((int)game->input.fingers[0].position.x > button->slider_min && (int)game->input.fingers[0].position.x < button->slider_max)
        {
            button->rect.x = game->input.fingers[0].position.x - button->rect.w / 2;
            
            float volume = ((float)game->input.fingers[0].position.x - (float)button->slider_min) / (float)button->slider_total;
            Mix_VolumeMusic(volume * 100);
        }
    }
}

void
DropMenu_create(UI *ui,
                IRect rect,
                int button_draw_spacing,
                int button_draw_margin,
                Texture *dropmenu_texture,
                Texture *default_button_texture,
                enum MenuState state)
{
    DropMenu *menu               = &ui->menus[ui->menu_amount];
    menu->ui                     = ui;
    menu->rect                   = rect;
    menu->button_draw_spacing    = button_draw_spacing;
    menu->button_draw_margin     = button_draw_margin;
    menu->dropmenu_texture       = dropmenu_texture;
    menu->pressed                = 0;
    menu->ui->button_amount      = 0;
    menu->default_button_texture = default_button_texture;
    menu->state                  = state;
    menu->next                   = 0;
    ui->menu_amount              += 1;
}

void
DropMenu_draw(DropMenu *menu, Game *game)
{
    int x,y;
    uint clip[4] = {(uint)menu->rect.x,(uint)menu->rect.y, (uint)menu->rect.w,(uint)menu->rect.h};

    x = 0;
    y = 780;

    if (menu->dropmenu_texture)
    {
        if(game->active_game_screen.ui.state == MENU_STATE_BATTLES1 || game->active_game_screen.ui.state == MENU_STATE_BATTLES2 || game->active_game_screen.ui.state == MENU_STATE_BATTLES3 || game->active_game_screen.ui.state == MENU_STATE_MISSIONS)
        {
            if (game->active_game_screen.ui.state != MENU_STATE_PAUSE)
            {
                SpriteBatch_drawSprite(&game->spritebatch,
                   Assets_getTexture(&game->assets, TEX_MENU_BACKGROUND), 0, 0, 0);
            }
        }
        else
        {
            SpriteBatch_drawSprite(&game->spritebatch,
                                   menu->dropmenu_texture, x, y, clip);
        }
    }

    if (menu->next)
    {
        Button *b = menu->next;
        Button_draw(b, game);
        LabelCheck(b, game);

        while (b->next)
        {
            b = b->next;
            Button_draw(b, game);
            LabelCheck(b, game);
        }
    }
}

void
LabelCheck(Button *b, Game *game)
{
    if (b->label_text && b->font_scale == 0)
    {
        SpriteBatch_drawSprite(&game->spritebatch, &b->label,
                               b->rect.x + b->rect.w / 2 - (int)b->label.width / 2, 
                               b->rect.y + b->rect.h / 2 - (int)b->label.height / 2, 
                               0);
    }
    else if (b->label_text && b->font_scale > 0)
    {
        SpriteBatch_drawSprite_Scale(&game->spritebatch, &b->label,
                               b->rect.x + b->rect.w / 2 - (int)b->label.width / 2 * b->font_scale, 
                               b->rect.y + b->rect.h / 2 - (int)b->label.height / 2 * b->font_scale, 
                               0, (float)b->font_scale, (float)b->font_scale);
    }
}

void
DropMenu_update(DropMenu *menu, Game *game)
{
    if (menu->next)
    {
        Button *b = menu->next;
        
        Button_update(b, menu->ui, game);
        
        while (b->next)
        {
            b = b->next;
            if (b->slider != 2)
            {
                Button_update(b, menu->ui, game);
            }
            
        }
    }
}

void
DropMenu_addButton(DropMenu *menu,
                   void (*onPress)(Button*, Game*),
                   void (*onRelease)(Button*, Game*),
                   Texture *texture, 
                   const char *label)
{
    Button *button = &menu->ui->buttons[menu->ui->button_amount];
    button->head = menu;
    IRect r;
    
    if (texture == 0)
    {
        texture = menu->default_button_texture;
    } 

    if (menu->state == MENU_STATE_NORMAL)
    {
        if (menu->button_amount == 0) //Pause menu
        {
            r = {RESOLUTION_WIDTH - 128, 0, 124, 88};
            Button_create(menu->ui, r, {texture, {611, 1252, 124, 88}}, {texture, {876, 1252, 124, 88}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = menu->ui->buttons[menu->ui->button_amount - 1].texar_normal;
        }
        else if (menu->button_amount == 1) //Back to menu
        {
            r = {0, 0, 256, 256};
            Button_create(menu->ui, r, {texture, {0, 0, 256, 256}}, {texture, {256, 0, 256, 256}}, onPress, onRelease, label, 0);
        }
        else if (menu->button_amount == 2) //Game start 
        {
            r = {RESOLUTION_WIDTH / 2 - 256 / 2, RESOLUTION_HEIGHT / 2 - 256 / 2, 256, 256};
            Button_create(menu->ui, r, {texture, {0, 0, 256, 256}}, {texture, {256, 0, 256, 256}}, onPress, onRelease, label, 0);
        }
    }
    else if (menu->state == MENU_STATE_MISSIONS)
    {
        if (menu->button_amount == 0) //Mission1
        {
            r = {(RESOLUTION_WIDTH - 816) / 2, 
             menu->rect.y + menu->button_draw_margin + menu->button_amount * (menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)) + menu->button_amount * menu->button_draw_margin,
             menu->rect.w - 2 * menu->button_draw_margin, menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)};
             
            Button_create(menu->ui, r, {texture, {1932, 30, 816, 216}}, {texture, {1061, 765, 816, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {1061, 30, 816, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 0;
        }
        if (menu->button_amount == 1) //Mission2
        {
            r = {(RESOLUTION_WIDTH - 816) / 2, 
             menu->rect.y + menu->button_draw_margin + menu->button_amount * (menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)) + menu->button_amount * menu->button_draw_margin,
             menu->rect.w - 2 * menu->button_draw_margin, menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)};
             
            Button_create(menu->ui, r, {texture, {1932, 275, 816, 216}}, {texture, {1061, 1010, 816, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {1061, 275, 816, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 0;
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
        }
        if (menu->button_amount == 2) //Mission3
        {
            r = {(RESOLUTION_WIDTH - 816) / 2, 
             menu->rect.y + menu->button_draw_margin + menu->button_amount * (menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)) + menu->button_amount * menu->button_draw_margin,
             menu->rect.w - 2 * menu->button_draw_margin, menu->rect.h / 3 - (menu->button_draw_margin + menu->button_draw_margin / 3)};
             
            Button_create(menu->ui, r, {texture, {1932, 520, 816, 216}}, {texture, {1061, 1255, 816, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {1061, 520, 816, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 0;
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
        }
        if (menu->button_amount == 3) //30fps
        {
            r = {RESOLUTION_WIDTH - 138 - 50, 50, 138, 138};
                 
            Button_create(menu->ui, r, {texture, {1990, 1873, 138, 138}}, {texture, {2227, 1873, 138, 138}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 0;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = -1;
        }
        if (menu->button_amount == 4) //60fps
        {
            r = {RESOLUTION_WIDTH - 138 - 50, 238, 138, 138};
                 
            Button_create(menu->ui, r, {texture, {1990, 1873, 138, 138}}, {texture, {2227, 1873, 138, 138}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 0;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = -2;
        }
    }
    else if (menu->state == MENU_STATE_PAUSE)
    {
        if (menu->button_amount == 0) //Continue
        {
            r = {(RESOLUTION_WIDTH - 716) / 2, 
             menu->rect.y + ((RESOLUTION_HEIGHT - menu->rect.y - 2 * 216) / 3),
             menu->rect.w - 2 * menu->button_draw_spacing, 150};
             
            Button_create(menu->ui, r, {texture, {1938, 765, 716, 216}}, {texture, {1938, 1010, 716, 216}}, onPress, onRelease, label, 0);
        }
        if (menu->button_amount == 1) //Quit battle
        {
            r = {(RESOLUTION_WIDTH - 716) / 2, 
             menu->rect.y + ((RESOLUTION_HEIGHT - menu->rect.y - 2 * 216) / 3 * 2 + 216),
             menu->rect.w - 2 * menu->button_draw_spacing, 150};
             
            Button_create(menu->ui, r, {texture, {1067, 1500, 716, 216}}, {texture, {1067, 1745, 716, 216}}, onPress, onRelease, label, 0);
        }
        
    }
    else if (menu->state == MENU_STATE_BATTLES1)
    {
        if (menu->button_amount == 0) //Battle 1
        {
            r = {(RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {31, 275, 216, 216}}, {texture, {31, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
        }
        else if (menu->button_amount == 1) //Battle 2
        {
            r = {(RESOLUTION_WIDTH - 3 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 3,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
            
            Button_create(menu->ui, r, {texture, {284, 275, 216, 216}}, {texture, {284, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {284, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 2;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
        }
        else if (menu->button_amount == 2) //Battle 3
        {
            r = {(RESOLUTION_WIDTH - 2 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 2,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {537, 275, 216, 216}}, {texture, {537, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {537, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 3;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
        }
        else if (menu->button_amount == 3) //Battle 4
        {
            r = {(RESOLUTION_WIDTH - 216) - (RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {790, 275, 216, 216}}, {texture, {790, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {790, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 4;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
        }
        else if (menu->button_amount == 4) //Boss
        {
            r = {(RESOLUTION_WIDTH - 400) / 2, 
                 menu->rect.y + menu->button_draw_margin * 2 + 216,
                 400, 276};
                 
            Button_create(menu->ui, r, {texture, {606, 765, 400, 276}}, {texture, {31, 1070, 400, 276}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 765, 400, 276}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 8055;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 1;
        }
        else if (menu->button_amount == 5) //Back
        {
            r = { 50,
                 RESOLUTION_HEIGHT - 50 - 150,
                 300, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1708, 264, 136}}, {texture, {637, 1708, 264, 136}}, onPress, onRelease, label, 0);
        }
        
        
    }
    else if (menu->state == MENU_STATE_BATTLES2)
    {
        if (menu->button_amount == 0) //Battle 1
        {
            r = {(RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {31, 275, 216, 216}}, {texture, {31, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
            //menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
        }
        else if (menu->button_amount == 1) //Battle 2
        {
            r = {(RESOLUTION_WIDTH - 3 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 3,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
            
            Button_create(menu->ui, r, {texture, {284, 275, 216, 216}}, {texture, {284, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {284, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 2;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
        }
        else if (menu->button_amount == 2) //Battle 3
        {
            r = {(RESOLUTION_WIDTH - 2 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 2,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {537, 275, 216, 216}}, {texture, {537, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {537, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 3;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
        }
        else if (menu->button_amount == 3) //Battle 4
        {
            r = {(RESOLUTION_WIDTH - 216) - (RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {790, 275, 216, 216}}, {texture, {790, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {790, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 4;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
        }
        else if (menu->button_amount == 4) //Boss
        {
            r = {(RESOLUTION_WIDTH - 400) / 2, 
                 menu->rect.y + menu->button_draw_margin * 2 + 216,
                 400, 276};
                 
            Button_create(menu->ui, r, {texture, {606, 765, 400, 276}}, {texture, {31, 1070, 400, 276}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 765, 400, 276}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 8055;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 2;
        }
        else if (menu->button_amount == 5) //Back
        {
            r = { 50,
                 RESOLUTION_HEIGHT - 50 - 150,
                 300, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1708, 264, 136}}, {texture, {637, 1708, 264, 136}}, onPress, onRelease, label, 0);
        }
        
        
    }
    else if (menu->state == MENU_STATE_BATTLES3)
    {
        if (menu->button_amount == 0) //Battle 1
        {
            r = {(RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {31, 275, 216, 216}}, {texture, {31, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
            //menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
        }
        else if (menu->button_amount == 1) //Battle 2
        {
            r = {(RESOLUTION_WIDTH - 3 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 3,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
            
            Button_create(menu->ui, r, {texture, {284, 275, 216, 216}}, {texture, {284, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {284, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 2;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
        }
        else if (menu->button_amount == 2) //Battle 3
        {
            r = {(RESOLUTION_WIDTH - 2 * 216) - (RESOLUTION_WIDTH - 4 * 216) / 5 * 2,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {537, 275, 216, 216}}, {texture, {537, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {537, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 3;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
        }
        else if (menu->button_amount == 3) //Battle 4
        {
            r = {(RESOLUTION_WIDTH - 216) - (RESOLUTION_WIDTH - 4 * 216) / 5,
                 menu->rect.y + menu->button_draw_margin,
                 216, 216};
                 
            Button_create(menu->ui, r, {texture, {790, 275, 216, 216}}, {texture, {790, 520, 216, 216}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {790, 30, 216, 216}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 4;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
        }
        else if (menu->button_amount == 4) //Boss
        {
            r = {(RESOLUTION_WIDTH - 400) / 2, 
                 menu->rect.y + menu->button_draw_margin * 2 + 216,
                 400, 276};
                 
            Button_create(menu->ui, r, {texture, {606, 765, 400, 276}}, {texture, {31, 1070, 400, 276}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].texar_blocked = {texture, {31, 765, 400, 276}};
            menu->ui->buttons[menu->ui->button_amount - 1].blocked = 1;
            menu->ui->buttons[menu->ui->button_amount - 1].battle_number = 8055;
            menu->ui->buttons[menu->ui->button_amount - 1].mission_number = 3;
        }
        else if (menu->button_amount == 5) //Back
        {
            r = { 50,
                 RESOLUTION_HEIGHT - 50 - 150,
                 300, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1708, 264, 136}}, {texture, {637, 1708, 264, 136}}, onPress, onRelease, label, 0);
        }
        
        
    }
    else if (menu->state == MENU_STATE_QUIT_CONFIRM)
    {
        if (menu->button_amount == 0) //Cancel
        {
            r = {(RESOLUTION_WIDTH - 2 * 264) / 3,
                 RESOLUTION_HEIGHT - menu->button_draw_margin - 350,
                 400, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1873, 264, 136}}, {texture, {637, 1873, 264, 136}}, onPress, onRelease, label, 0);
        }
        if (menu->button_amount == 1) //Confirm
        {
            r = {(RESOLUTION_WIDTH - 2 * 264) / 3 * 2 + 264,
                 RESOLUTION_HEIGHT - menu->button_draw_margin - 350,
                 400, 150};
            
            Button_create(menu->ui, r, {texture, {334, 1376, 264, 136}}, {texture, {637, 1376, 264, 136}}, onPress, onRelease, label, 0);
        }
        if (menu->button_amount == 2) //"Quit battle" text
        {
            r = {RESOLUTION_WIDTH / 2 - 400 / 2,
                 menu->rect.y + 300,
                 400, 150};
                 
            Button_create(menu->ui, r, {0, {0, 0, 0, 0}}, {0, {0, 0, 0, 0}}, onPress, onRelease, label, 2);
        }
    }
    else if (menu->state == MENU_STATE_GAMEOVER)
    {
        r = {menu->rect.x + menu->button_draw_spacing, 
             menu->rect.y + menu->button_draw_margin + menu->button_amount * (150 + (RESOLUTION_HEIGHT - menu->rect.y - 2 * 150 - 2 * menu->button_draw_margin)),
             menu->rect.w - 2 * menu->button_draw_spacing, 150};
             
        Button_create(menu->ui, r, {texture, {0, 0, (uint)r.w, (uint)r.h}}, {texture, {0, 0, 0, 0}}, onPress, onRelease, label, 0);
    }
    else if (menu->state == MENU_STATE_OPTIONS)
    {
        if (menu->button_amount == 0) //Back
        {
            r = {menu->button_draw_margin,
                 RESOLUTION_HEIGHT - menu->button_draw_margin - 150,
                 300, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1708, 264, 136}}, {texture, {637, 1708, 264, 136}}, onPress, onRelease, label, 0);
        }
        else if (menu->button_amount == 1) //Volume text
        {
            r = {menu->button_draw_margin / 5 * 4,
                 menu->rect.y + menu->button_draw_margin * 2,
                 300, 150};
                 
            Button_create(menu->ui, r, {0, {0, 0, 0, 0}}, {0, {0, 0, 0, 0}}, onPress, onRelease, label, 0);
        }
        else if (menu->button_amount == 2) //Slider
        {
            r = {menu->button_draw_margin,
                 menu->rect.y + menu->button_draw_margin * 6,
                 RESOLUTION_WIDTH - 2 * menu->button_draw_margin, 100};
                 
            Button_create(menu->ui, r, {texture, {1938, 1766, 944, 44}}, {texture, {1938, 1766, 944, 44}}, onPress, onRelease, label, 0);
            menu->ui->buttons[menu->ui->button_amount - 1].slider = 2;
        }
        else if (menu->button_amount == 3) //Slider button
        {
            menu->ui->buttons[menu->ui->button_amount].slider_min = 160;
            menu->ui->buttons[menu->ui->button_amount].slider_max = RESOLUTION_WIDTH - 180;
            menu->ui->buttons[menu->ui->button_amount].slider_total = menu->ui->buttons[menu->ui->button_amount].slider_max - menu->ui->buttons[menu->ui->button_amount].slider_min;
            r = {(int)(Mix_VolumeMusic(-1) / 100 * menu->ui->buttons[menu->ui->button_amount].slider_total), menu->rect.y + menu->button_draw_margin * 5, 150, 150};
            Button_create(menu->ui, r, {texture, {1990, 1873, 138, 138}}, {texture, {2227, 1873, 138, 138}}, onPress, onRelease, label, 0);
        }
    }
    else if (menu->state == MENU_STATE_VICTORY)
    {
        if (menu->button_amount == 0) //Points
        {
            r = {menu->button_draw_margin / 5 * 4,
                 menu->rect.y + menu->button_draw_margin * 2,
                 300, 150};
                 
            Button_create(menu->ui, r, {0, {0, 0, 0, 0}}, {0, {0, 0, 0, 0}}, onPress, onRelease, label, 0);
        }
        else if (menu->button_amount == 1) //Next
        {
            r = {RESOLUTION_WIDTH - menu->button_draw_margin - 300,
                 RESOLUTION_HEIGHT - menu->button_draw_margin - 150,
                 300, 150};
                 
            Button_create(menu->ui, r, {texture, {334, 1541, 264, 136}}, {texture, {637, 1541, 264, 136}}, onPress, onRelease, label, 0);
        }
    }
    else if (menu->state == MENU_STATE_START)
    {
        r = {RESOLUTION_WIDTH /2 - 464 /2, RESOLUTION_HEIGHT - 501, 464, 101};
                 
        Button_create(menu->ui, r, {0, {0, 0, 0, 0}}, {0, {0, 0, 0, 0}}, onPress, onRelease, label, 2);
    }
    else if (menu->state == MENU_STATE_ERROR)
    {
        if (menu->button_amount == 0) //"Could not" text
        {
            r = {RESOLUTION_WIDTH / 2 - 400 / 2,
                 menu->rect.y + 300,
                 400, 150};
                 
            Button_create(menu->ui, r, {0, {0, 0, 0, 0}}, {0, {0, 0, 0, 0}}, onPress, onRelease, label, 2);
        }
        else if (menu->button_amount == 1) //Confirm
        {
            r = {(RESOLUTION_WIDTH - 2 * 264) / 3 * 2 + 264,
                 RESOLUTION_HEIGHT - menu->button_draw_margin - 350,
                 400, 150};
            
            Button_create(menu->ui, r, {texture, {334, 1376, 264, 136}}, {texture, {637, 1376, 264, 136}}, onPress, onRelease, label, 0);
        }
    }

    if (!menu->next)
    {
        menu->next = button;
    }
    
    else
    {
        Button *b = menu->next;
        
        while (b->next)
        {
            b = b->next;
        }
        b->next = button;
    }        
    
    if (label != 0)
    {
        button->label_text = label;
    }
    
    menu->button_amount += 1;
}
/*
    IRect r = {menu->rect.x + menu->draw_margin, 
                   menu->rect.y + menu->draw_margin + menu->ui.button_amount * 100 + menu->ui.button_amount * menu->draw_margin,
                   menu->rect.w - 2 * menu->draw_margin, 100};
                   */

void
Menu_state_check(UI *ui)
{
    for (int i = 0; i < ui->menu_amount; i++)
    {
        if (ui->menus[i].state == ui->state)
            ui->menus[i].pressed = 1;
        else
            ui->menus[i].pressed = 0;
    }
}

void
UI_draw(UI *ui, Game *game)
{
    //Menu_state_check(ui);

    if (ui->button_amount != 0)
    {
        for (int i = 0; i < ui->button_amount; i++)
        {
            if (!ui->buttons[i].head)
            {
                Button_draw(&ui->buttons[i], game);
            }
        }
    }

    if (ui->menu_amount != 0)
    {
        for (int i = 0; i < ui->menu_amount; i++)
        {
            if (ui->menus[i].pressed == 1)
            {
                DropMenu_draw(&ui->menus[i], game);
            }
        }
    }
}

void
UI_update(UI *ui, Game *game)
{
    Menu_state_check(ui);
    
    if (ui->button_amount != 0)
    {
        for (int i = 0; i < ui->button_amount; i++)
        {
            if (!ui->buttons[i].head)
            {
                Button_update(&ui->buttons[i], ui, game);
            }
        }
    }
    
    if (ui->menu_amount != 0)
    {
        for (int i = 0; i < ui->menu_amount; i++)
        {
            if (ui->menus[i].pressed == 1)
            {
                DropMenu_update(&ui->menus[i], game);
            }
        }
    }
}

void
ProgressBar_create(ProgressBar *bar, 
                   TextureArea *txa_background, int bg_offset_x, int bg_offset_y,
                   TextureArea *txa_bar, int bar_offset_x, int bar_offset_y,
                   TextureArea *txa_border, int border_offset_x, int border_offset_y,
                   int x, int y, int w, int h, int max, int cur)
{
    bar->txa_bar            = txa_bar;
    bar->txa_background     = txa_background;
    bar->txa_border         = txa_border;
    bar->bar_offset.x       = bar_offset_x;
    bar->bar_offset.y       = bar_offset_y;
    bar->bg_offset.x        = bg_offset_x;
    bar->bg_offset.y        = bg_offset_y;
    bar->border_offset.x    = border_offset_x;
    bar->border_offset.y    = border_offset_y;
    bar->rect.x             = x;
    bar->rect.y             = y;
    bar->rect.w             = w;
    bar->rect.h             = h;
}

void
ProgressBar_draw(ProgressBar *bar, Game *game, float scalex, float scaley, int min, int max, int cur)
{
    if (bar->txa_background->texture)
    {
        SpriteBatch_drawSprite_Scale(&game->spritebatch, bar->txa_background->texture,
            bar->rect.x + bar->bg_offset.x, bar->rect.y + bar->bg_offset.y, bar->txa_background->clip, scalex, scaley);
    }

    if (bar->txa_bar->texture)
    {
        uint clip[4] = { bar->txa_bar->clip[0], bar->txa_bar->clip[1], (uint)((float)cur / (float)max * (float)bar->txa_bar->clip[2]),  bar->txa_bar->clip[3] };
        SpriteBatch_drawSprite_Scale(&game->spritebatch, bar->txa_bar->texture,
            bar->rect.x + bar->bar_offset.x, bar->rect.y + bar->bar_offset.y, clip, scalex, scaley);
    }

    if (bar->txa_border->texture)
    {
        SpriteBatch_drawSprite_Scale(&game->spritebatch, bar->txa_border->texture,
            bar->rect.x + bar->border_offset.x, bar->rect.y + bar->border_offset.y, bar->txa_border->clip, scalex, scaley);
    }
}
